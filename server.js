require('dotenv').config()

const express = require('express')
const app = express()
require('./connections/dbConnections')
const port = process.env.PORT
const nodemailer = require('nodemailer')

app.use(express.json())
app.use('/', require('./routes/index'))


// * is a wild card character
app.get('*', (req, res) => {
    res.status(404).send({ status: 404, message: 'requested route does exist' })
})


app.delete('*', (req, res) => {
    res.status(404).send({ status: 404, message: 'Requested route does exist' })
})

app.put('*', (req, res) => {
    res.status(404).send({ status: 404, message: 'Requested route does exist' })
})

app.post('*', (req, res) => {
    res.status(404).send({ status: 404, message: 'Requested route does exist' })
})




app.listen(port, (error) => {
    if (error) {
        return console.log(error)
    }
    console.log('Server is up and running on port ' + port);
})
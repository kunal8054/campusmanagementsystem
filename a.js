app.get('/yt2', async (req, res) => {

    const data = await Category.aggregate([
        {
            $lookup: {
                from: 'facultysubjects',
                as: 'facultySubjects',
                let: { facultysubjectsid: '$_id' },
                pipeline: [
                    {
                        $match: {
                            $expr: { $eq: ['$faculty', '$$facultysubjectsid'] }

                        }
                    },
                    {
                        $lookup: {
                            from: 'subjects',
                            as: 'subjects',
                            let: { subjectsid: '$_id' },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr: { $eq: ['$subject', '$$subjectsid'] }
                                    }
                                }
                            ]

                        }
                    }
                ]

            }
        }
    ])

    res.send(data)

})



    // const facultyProfile = await Faculty.aggregate([
    //     {
    //         $match: { _id: req.faculty, isDeleted: false }
    //     },
    //     {
    //         $lookup: {
    //             from: 'facultysubjects',
    //             as: 'facultySubjects',
    //             let: { facultysubjectsid: '$_id' },
    //             pipeline: [
    //                 {
    //                     $match: {
    //                         $expr: { $eq: ['$faculty', '$$facultysubjectsid'] }

    //                     }
    //                 },
    //                 {
    //                     $lookup: {
    //                         from: 'subjects',
    //                         as: 'subjects',
    //                         let: { subjectsid: '$_id' },
    //                         pipeline: [
    //                             {
    //                                 $match:
    //                                 {
    //                                     $expr: { $eq: ['$subject', '$$subjectsid'] }
    //                                 }
    //                             }
    //                         ]

    //                     }
    //                 }
    //             ]

    //         }
    //     },
    //     {
    //         $project: { isDeleted: 0, createdAt: 0, updatedAt: 0, __v: 0, password: 0, token: 0 }
    //     }
    // ])